package com.example.tarea3recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private List<MiFila> datosDeportes;
    private Button buttonAceptar;
    private AdaptadorMisFilas adaptadorMisFilas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonAceptar = findViewById(R.id.button);

        setEscuchadores();

        datosDeportes = new ArrayList<MiFila>();
        datosDeportes.add(new MiFila("Baloncesto", R.mipmap.ic_baloncesto));
        datosDeportes.add(new MiFila("Fútbol", R.mipmap.ic_futbol));
        datosDeportes.add(new MiFila("Motociclismo", R.mipmap.ic_moto));
        datosDeportes.add(new MiFila("Natación", R.mipmap.ic_natacion));
        datosDeportes.add(new MiFila("Golf", R.mipmap.ic_golf));
        datosDeportes.add(new MiFila("Atletismo", R.mipmap.ic_atletismo));

        RecyclerView recyclerView = findViewById(R.id.recyclerDeporte);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adaptadorMisFilas = new AdaptadorMisFilas(datosDeportes);

        recyclerView.setAdapter(adaptadorMisFilas);
    }

    private void setEscuchadores() {
        buttonAceptar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        ArrayList<String> deportesSeleccionados = adaptadorMisFilas.getChecked_items();

        String deportes = "";

        for (String deportesSeleccionado : deportesSeleccionados) {
            deportes += deportesSeleccionado + "\n";
        }

        Toast.makeText(getBaseContext(), "Los deportes seleccionados son : " + deportes, Toast.LENGTH_LONG).show();
    }
}