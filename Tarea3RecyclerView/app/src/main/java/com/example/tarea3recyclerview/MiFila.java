package com.example.tarea3recyclerview;

import android.widget.CheckBox;

public class MiFila {

    private String nombreDeporte;
    private int imagenDeporte;

    public MiFila(String nombreDeporte, int imagenDeporte) {
        this.nombreDeporte = nombreDeporte;
        this.imagenDeporte = imagenDeporte;
    }

    public String getNombreDeporte() {
        return nombreDeporte;
    }

    public void setNombreDeporte(String nombreDeporte) {
        this.nombreDeporte = nombreDeporte;
    }

    public int getImagenDeporte() {
        return imagenDeporte;
    }

    public void setImagenDeporte(int imagenDeporte) {
        this.imagenDeporte = imagenDeporte;
    }
}
