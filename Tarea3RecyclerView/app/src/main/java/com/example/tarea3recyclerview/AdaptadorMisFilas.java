package com.example.tarea3recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class AdaptadorMisFilas extends RecyclerView.Adapter<AdaptadorMisFilas.ViewHolder> {

    List<MiFila> datos;
    ArrayList<String> checked_items;

    public AdaptadorMisFilas(List<MiFila> datos) {
        this.datos = datos;
        checked_items = new ArrayList<>();
    }

    public ArrayList<String> getChecked_items() {
        return checked_items;
    }

    public void setChecked_items(ArrayList<String> checked_items) {
        this.checked_items = checked_items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fila, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String nombreDeporte = datos.get(position).getNombreDeporte();
        int imagenDeporte = datos.get(position).getImagenDeporte();

        holder.nombre.setText(nombreDeporte);
        holder.imagen.setImageResource(imagenDeporte);

        holder.checkBox.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (compoundButton.isChecked()) {
                            compoundButton.setChecked(true);
                            checked_items.add(datos.get(position).getNombreDeporte());
                        } else {
                            compoundButton.setChecked(false);
                            checked_items.remove(datos.get(position).getNombreDeporte());
                        }
                    }
                }
        );
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nombre;
        private ImageView imagen;
        private CheckBox checkBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombreDeporte);
            imagen = itemView.findViewById(R.id.imagenDeporte);
            checkBox = itemView.findViewById(R.id.checkBox);
        }
    }
}
